from aiogram.utils.callback_data import CallbackData

start_cb = CallbackData("start_callback", "marker")
salon_cb = CallbackData("salon_callback", "salon")
calendar_cb = CallbackData("calendar_callback", "action", "year", "month", "day")
# ------------------- merge -------------------------------
category_cb = CallbackData("category_callback", "action")
subcategory_cb = CallbackData("subcategory_callback", "action")
# ---------------------------------------------------------
service_cb = CallbackData("service_callback", "action")
time_cb = CallbackData("time_callback", "action")
