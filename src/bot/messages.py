import callbacks
from abc import ABC
from aiogram import types
import datetime
import calendar


class BaseMessage(ABC):
    pass


class StartMessage(BaseMessage):

    def __init__(self, telegram_name: str) -> None:
        self.telegram_name = telegram_name

        # local variables
        self._greeting_text = self._create_greeting_text()
        self._keyboard_action_text = self._create_keyboard_action_text()
        self._keyboard = self._create_keyboard()

    @property
    def greeting_text(self) -> str:
        return self._greeting_text

    @property
    def keyboard_action_text(self) -> str:
        return self._keyboard_action_text

    @property
    def keyboard(self) -> types.InlineKeyboardMarkup:
        return self._keyboard

    def _create_keyboard(self) -> types.InlineKeyboardMarkup:
        """
        Should move to choose salon
        :return:
        """

        keyboard_markup = types.InlineKeyboardMarkup()
        keyboard_markup.add(
            types.InlineKeyboardButton("Перейти к выбору салона",
                                       callback_data=callbacks.start_cb.new(marker="to_salon"))
        )
        return keyboard_markup

    def _create_greeting_text(self) -> str:
        """
        :return:
        """
        return f"""{self.telegram_name}, приветствую Вас‍"""

    def _create_keyboard_action_text(self) -> str:
        return """Пример текста приветственного сообщения"""


class SalonMessage(BaseMessage):

    def __init__(self) -> None:
        # local variables
        self._text = self._create_text()
        self._keyboard = self._create_keyboard()

    @property
    def text(self) -> str:
        return self._text

    @property
    def keyboard(self) -> types.InlineKeyboardMarkup:
        return self._keyboard

    def _create_keyboard(self) -> types.InlineKeyboardMarkup:
        keyboard_markup = types.InlineKeyboardMarkup()
        keyboard_markup.add(
            types.InlineKeyboardButton("Салон 1", callback_data=callbacks.salon_cb.new(salon="salon_1")),
            types.InlineKeyboardButton("Салон 2", callback_data=callbacks.salon_cb.new(salon="salon_2"))

        )
        return keyboard_markup

    def _create_text(self) -> str:
        return """Пример текста салонов."""


class MainMenuMessage(BaseMessage):

    def __init__(self) -> None:
        self._text = self._create_text()
        self._keyboard = self._create_keyboard()

    @property
    def text(self) -> str:
        return self._text

    @property
    def keyboard(self) -> types.ReplyKeyboardMarkup:
        return self._keyboard

    def _create_keyboard(self) -> types.ReplyKeyboardMarkup:
        keyboard_markup = types.ReplyKeyboardMarkup(row_width=3, resize_keyboard=True)
        keyboard_markup.row(
            types.KeyboardButton("Выборка 1"),
            types.KeyboardButton("Выборка 2"),
            types.KeyboardButton("Выборка 3")
        )
        keyboard_markup.add(
            types.KeyboardButton("О нас")
        )
        return keyboard_markup

    def _create_text(self) -> str:
        return """Пример текста главного меню."""


class CategoryMessage(BaseMessage):
    def __init__(self) -> None:
        self._text = self._create_text()
        self._keyboard = self._create_keyboard()

    @property
    def text(self) -> str:
        return self._text

    @property
    def keyboard(self) -> types.InlineKeyboardMarkup:
        return self._keyboard

    def _create_keyboard(self) -> types.InlineKeyboardMarkup:
        keyboard_markup = types.InlineKeyboardMarkup(row_width=3, resize_keyboard=True)
        keyboard_markup.add(
            types.InlineKeyboardButton("Пример подкатегории",
                                       callback_data=callbacks.category_cb.new(action="next_state")),
        )
        return keyboard_markup

    def _create_text(self) -> str:
        return """Пример текста категории."""


class SubCategoryMessage(BaseMessage):
    def __init__(self) -> None:
        self._text = self._create_text()
        self._keyboard = self._create_keyboard()

    @property
    def text(self) -> str:
        return self._text

    @property
    def keyboard(self) -> types.InlineKeyboardMarkup:
        return self._keyboard

    def _create_keyboard(self) -> types.InlineKeyboardMarkup:
        keyboard_markup = types.InlineKeyboardMarkup(row_width=3, resize_keyboard=True)
        keyboard_markup.add(
            types.InlineKeyboardButton("Пример подкатегории",
                                       callback_data=callbacks.subcategory_cb.new(action="next_state")),
        )
        keyboard_markup.add(
            types.InlineKeyboardButton("Назад", callback_data=callbacks.subcategory_cb.new(action="back")),
        )
        return keyboard_markup

    def _create_text(self) -> str:
        return """Пример текста подкатегории."""


class ServiceMessage(BaseMessage):
    def __init__(self) -> None:
        self._text = self._create_text()
        self._keyboard = self._create_keyboard()

    @property
    def text(self) -> str:
        return self._text

    @property
    def keyboard(self) -> types.InlineKeyboardMarkup:
        return self._keyboard

    def _create_keyboard(self) -> types.InlineKeyboardMarkup:
        keyboard_markup = types.InlineKeyboardMarkup(row_width=3, resize_keyboard=True)
        keyboard_markup.add(
            types.InlineKeyboardButton("Пример описания услуги",
                                       callback_data=callbacks.service_cb.new(action="next_state")),
        )
        keyboard_markup.add(
            types.InlineKeyboardButton("Назад", callback_data=callbacks.service_cb.new(action="back")),
        )
        return keyboard_markup

    def _create_text(self) -> str:
        return """Пример текста подкатегории."""


class CardMessage(BaseMessage):
    def __init__(self) -> None:
        self._text = self._create_text()
        self._keyboard = self._create_keyboard()

    @property
    def text(self) -> str:
        return self._text

    @property
    def keyboard(self) -> types.InlineKeyboardMarkup:
        return self._keyboard

    def _create_keyboard(self, year=None, month=None):
        pass

    def _create_text(self) -> str:
        return """Пример текста."""


class CalendarMessage(BaseMessage):
    def __init__(self, year=None, month=None) -> None:
        self._text = self._create_text()
        self._keyboard = self._create_keyboard(year, month)

    @property
    def text(self) -> str:
        return self._text

    @property
    def keyboard(self) -> types.InlineKeyboardMarkup:
        return self._keyboard

    def _create_keyboard(self, year=None, month=None):
        now = datetime.datetime.now()
        if year is None:
            year = now.year
        if month is None:
            month = now.month
        data_ignore = callbacks.calendar_cb.new(action="IGNORE", year=year, month=month, day=0)
        keyboard = []
        row = [types.InlineKeyboardButton(calendar.month_name[month] + " " + str(year), callback_data=data_ignore)]
        keyboard.append(row)
        row = []
        for day in ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"]:
            row.append(types.InlineKeyboardButton(day, callback_data=data_ignore))
        keyboard.append(row)

        my_calendar = calendar.monthcalendar(year, month)
        for week in my_calendar:
            row = []
            for day in week:
                if day == 0:
                    row.append(types.InlineKeyboardButton(" ", callback_data=data_ignore))
                else:
                    row.append(
                        types.InlineKeyboardButton(str(day),
                                                   callback_data=callbacks.calendar_cb.new(action="DAY", year=year,
                                                                                           month=month, day=day)))
            keyboard.append(row)
        row = [types.InlineKeyboardButton("<", callback_data=callbacks.calendar_cb.new(action="PREV-MONTH", year=year,
                                                                                       month=month, day=day)),
               types.InlineKeyboardButton(" ", callback_data=data_ignore),
               types.InlineKeyboardButton(">", callback_data=callbacks.calendar_cb.new(action="NEXT-MONTH", year=year,
                                                                                       month=month, day=day))]
        keyboard.append(row)
        return types.InlineKeyboardMarkup(row_width=31, inline_keyboard=keyboard)

    def _create_text(self) -> str:
        return """Пример календарного текста."""


class TimeMessage(BaseMessage):
    def __init__(self):
        self._text = self._create_text()
        self._keyboard = self._create_keyboard()

    @property
    def text(self) -> str:
        return self._text

    @property
    def keyboard(self) -> types.InlineKeyboardMarkup:
        return self._keyboard

    def _create_keyboard(self):
        keyboard_markup = types.InlineKeyboardMarkup(row_width=3, resize_keyboard=True)
        for i in range(0, 25):
            keyboard_markup.add(
                types.InlineKeyboardButton(f"0{i}:00")
            )
        return keyboard_markup

    def _create_text(self) -> str:
        return """Пример простого текта"""


class PhoneNumberMessage(BaseMessage):
    def __init__(self) -> None:
        self._text = self._create_text()

    @property
    def text(self) -> str:
        return self._text

    def _create_text(self) -> str:
        return """Введите номер телефона в следующем формате (+70000000000)"""


class NameMessage(BaseMessage):
    def __init__(self) -> None:
        self._success_text = self._create_success_text()
        self._fail_text = self._create_fail_text()

    @property
    def success_text(self) -> str:
        return self._success_text

    @property
    def fail_text(self) -> str:
        return self._fail_text

    def _create_success_text(self) -> str:
        return """Отлично! А теперь введите имя. """

    def _create_fail_text(self) -> str:
        return """Ошибка формата ввода. """
