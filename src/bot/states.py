from aiogram.dispatcher.filters.state import StatesGroup, State


class Begin(StatesGroup):
    StartMessage = State()
    SalonMessage = State()
    MainMenuMessage = State()
    CategoryState = State()
    SubcategoryState = State()
    ServiceState = State()
    EnterDate = State()
    EnterName = State()
    EnterPhoneNumber = State()
