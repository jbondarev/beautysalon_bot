import os
import aiohttp


class _Config:

    def __init__(self) -> None:
        self._api_token = os.getenv("TELEGRAM_API_TOKEN")
        self._proxy_url = os.getenv("TELEGRAM_PROXY_URL")
        self._proxy_auth = aiohttp.BasicAuth(
            login=os.getenv("TELEGRAM_PROXY_LOGIN"),
            password=os.getenv("TELEGRAM_PROXY_PASSWORD")
        )
        self._access_id = os.getenv("TELEGRAM_ACCESS_ID")

    @property
    def api_token(self) -> str:
        return self._api_token

    @property
    def proxy_url(self) -> str:
        return self._proxy_url

    @property
    def proxy_auth(self) -> aiohttp.BasicAuth:
        return self._proxy_auth

    @property
    def access_id(self) -> str:
        return self._access_id


config_object = _Config()
