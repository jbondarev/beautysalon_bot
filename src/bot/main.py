import asyncio
import datetime
import logging

import states
import callbacks
import messages
import phonenumbers
import sched

from aiogram.contrib.fsm_storage.memory import MemoryStorage
from config import config_object
from aiogram import Bot, Dispatcher, executor, types

logging.basicConfig(level=logging.INFO)
bot = Bot(token=config_object.api_token,
          proxy=config_object.proxy_url,
          proxy_auth=config_object.proxy_auth)
storage = MemoryStorage()
dispatcher = Dispatcher(bot, storage=storage)


@dispatcher.message_handler(commands=["start", "help"], state="*")
async def start_cmd_handler(message: types.Message):
    logging.log(logging.INFO, "start_cmd_handler: \n{0}".format(dispatcher.current_state()))
    user_id = message.from_user.id
    start_message = messages.StartMessage(message.from_user.first_name)
    await bot.send_message(user_id, text=start_message.greeting_text)
    await bot.send_message(user_id, text=start_message.keyboard_action_text, reply_markup=start_message.keyboard)
    await states.Begin.SalonMessage.set()


@dispatcher.callback_query_handler(callbacks.start_cb.filter(marker=["to_salon"]), state=states.Begin.SalonMessage)
async def choose_salon_handler(callback: types.CallbackQuery, callback_data: dict):
    logging.log(logging.INFO, "choose_salon_handler: \n{0}\n{1}".format(dispatcher.current_state(), callback_data))
    salon_message = messages.SalonMessage()
    await bot.send_message(callback.from_user.id, text=salon_message.text, reply_markup=salon_message.keyboard)
    await states.Begin.MainMenuMessage.set()


@dispatcher.callback_query_handler(callbacks.salon_cb.filter(), state=states.Begin.MainMenuMessage)
async def menu_handler(callback: types.CallbackQuery, callback_data: dict):
    logging.log(logging.INFO,
                "choose_salon_handler: \n{0}\n{1}\n{2}".format(dispatcher.current_state(), callback.as_json(),
                                                               callback_data))
    main_menu_message = messages.MainMenuMessage()

    # Should save user data, for get http request for our api
    data = {"get-request": "some-get-request/{}/".format(callback_data["salon"])}
    await storage.set_data(chat=callback.from_user.id, user=callback.from_user.id, data=data)
    log_data = await storage.get_data(chat=callback.from_user.id, user=callback.from_user.id)
    logging.log(logging.INFO, log_data)
    # ---------------------------------

    await bot.send_message(callback.from_user.id, text=main_menu_message.text, reply_markup=main_menu_message.keyboard)
    await states.Begin.CategoryState.set()


# Should be merged in one controller ---------------------------------

@dispatcher.message_handler(state=states.Begin.CategoryState)
async def choose_offers_category_handler(message: types.Message):
    """
    Should be MessageKeyboard
    :param message:
    :return:
    """
    logging.log(logging.INFO, "choose_offers_category_handler: \n{0}".format(dispatcher.current_state()))
    category_message = messages.CategoryMessage()
    await bot.send_message(chat_id=message.from_user.id, text=category_message.text,
                           reply_markup=category_message.keyboard)
    await states.Begin.CategoryState.set()


@dispatcher.callback_query_handler(callbacks.category_cb.filter(), state=states.Begin.CategoryState)
async def choose_offers_category_handler_callback(callback: types.CallbackQuery, callback_data: dict):
    logging.log(logging.INFO, "choose_offers_category_handler: \n{0}".format(dispatcher.current_state()))
    if callback_data["action"] == "next_state":
        sub_category_message = messages.SubCategoryMessage()
        await bot.send_message(chat_id=callback.from_user.id, text=sub_category_message.text,
                               reply_markup=sub_category_message.keyboard)
        await states.Begin.SubcategoryState.set()


@dispatcher.callback_query_handler(callbacks.subcategory_cb.filter(), state=states.Begin.SubcategoryState)
async def choose_offers_subcategory_handler(callback: types.CallbackQuery, callback_data: dict):
    logging.log(logging.INFO, "choose_offers_subcategory_handler: \n{0}".format(dispatcher.current_state()))
    if callback_data["action"] == "back":
        category_message = messages.CategoryMessage()
        await bot.send_message(chat_id=callback.from_user.id, text=category_message.text,
                               reply_markup=category_message.keyboard)
        await states.Begin.CategoryState.set()
    else:
        service_message = messages.ServiceMessage()
        await bot.send_message(chat_id=callback.from_user.id, text=service_message.text,
                               reply_markup=service_message.keyboard)
        await states.Begin.ServiceState.set()


@dispatcher.callback_query_handler(callbacks.service_cb.filter(), state=states.Begin.ServiceState)
async def service_handler(callback: types.CallbackQuery, callback_data: dict):
    logging.log(logging.INFO, "service_handler: \n{0}".format(dispatcher.current_state()))
    if callback_data["action"] == "back":
        sub_category_message = messages.SubCategoryMessage()
        await bot.send_message(chat_id=callback.from_user.id, text=sub_category_message.text,
                               reply_markup=sub_category_message.keyboard)
        await states.Begin.SubcategoryState.set()
    else:
        calendar_message = messages.CalendarMessage()
        logging.log(logging.INFO,
                    "choose_service_handler: \n{0}\n{1}\n".format(dispatcher.current_state(),
                                                                  calendar_message.keyboard))
        await bot.send_message(callback.from_user.id, text=calendar_message.text,
                               reply_markup=calendar_message.keyboard)
        await states.Begin.EnterDate.set()
        await states.Begin.EnterDate.set()


# --------------------------------------------------------------------------


@dispatcher.callback_query_handler(callbacks.calendar_cb.filter(), state=states.Begin.EnterDate)
async def calendar_handler(callback: types.CallbackQuery, callback_data: dict):
    logging.log(logging.INFO,
                "choose_salon_handler: \n{0}\n{1}\n{2}".format(dispatcher.current_state(), callback.as_json(),
                                                               callback_data))
    (action, year, month, day) = callback_data["action"], callback_data["year"], callback_data["month"], callback_data[
        "day"]
    # TODO move logic to controller class with buiseness logic (messages.py)
    curr = datetime.datetime(int(year), int(month), 1)
    if action == "IGNORE":
        await bot.answer_callback_query(callback_query_id=callback.id)
    elif action == "DAY":
        await bot.edit_message_text(
            text=f"Вы выбрали следующую дату: {callback_data['year']}\{callback_data['month']}\{callback_data['day']}",
            chat_id=callback.from_user.id,
            message_id=callback.message.message_id)
        phone_number_message = messages.PhoneNumberMessage()
        await bot.send_message(text=phone_number_message.text, chat_id=callback.from_user.id)
        await states.Begin.EnterPhoneNumber.set()
    elif action == "PREV-MONTH":
        pre = curr - datetime.timedelta(days=1)
        await bot.edit_message_text(text=callback.message.text,
                                    chat_id=callback.from_user.id,
                                    message_id=callback.message.message_id,
                                    reply_markup=messages.CalendarMessage(int(pre.year), int(pre.month)).keyboard)
    elif action == "NEXT-MONTH":
        ne = curr + datetime.timedelta(days=31)
        await bot.edit_message_text(text=callback.message.text,
                                    chat_id=callback.from_user.id,
                                    message_id=callback.message.message_id,
                                    reply_markup=messages.CalendarMessage(int(ne.year), int(ne.month)).keyboard)
    else:
        raise Exception("Something went wrong")


@dispatcher.message_handler(state=states.Begin.EnterPhoneNumber)
async def phone_number_handler(message: types.Message):
    # TODO move logic to controller class with buiseness logic (messages.py)
    name_message = messages.NameMessage()
    phone_number = None
    try:
        phone_number = phonenumbers.parse(message.text, "RU")
    except:
        pass
    if phone_number is not None:
        if phonenumbers.is_valid_number(phonenumbers.parse(message.text, "RU")):
            # TODO save to db instance? send to API?
            await bot.send_message(text=name_message.success_text, chat_id=message.from_user.id)
            await states.Begin.EnterName.set()
        else:
            await bot.send_message(text=name_message.fail_text, chat_id=message.from_user.id)
    else:
        await bot.send_message(text=name_message.fail_text, chat_id=message.from_user.id)


# @dispatcher.message_handler(state=states.Begin.EnterDate)
# async def choose_service_time(message: types.Message):
#     """
#     Should be MessageKeyboard
#     :param message:
#     :return:
#     """
#     calendar_message = messages.CalendarMessage()
#     logging.log(logging.INFO,
#                 "choose_service_handler: \n{0}\n{1}\n".format(dispatcher.current_state(), calendar_message.keyboard))
#     await bot.send_message(message.from_user.id, text=calendar_message.text, reply_markup=calendar_message.keyboard)
#     await states.Begin.EnterDate.set()


@dispatcher.message_handler(state=states.Begin.EnterName)
async def name_handler(message: types.Message):
    # TODO save to db
    await bot.send_message(text="Ваша заявка записана", chat_id=message.from_user.id)
    await states.Begin.MainMenuMessage.set()
    import aiojobs
    scheduler = await aiojobs.create_scheduler()
    await scheduler.spawn(coro(message))
    await asyncio.sleep(10.0)
    await scheduler.close()


async def coro(message: types.Message):
    await asyncio.sleep(5.0)
    await bot.send_message(text="Вы записаны", chat_id=message.from_user.id)


if __name__ == "__main__":
    executor.start_polling(dispatcher, skip_updates=True)
