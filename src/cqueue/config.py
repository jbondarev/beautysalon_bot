from celery import Celery

app = Celery("config", backend="amqp", broker="amqp://")
