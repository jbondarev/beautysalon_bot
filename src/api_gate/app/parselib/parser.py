import time
from selenium.webdriver.support import expected_conditions as EC
from typing import List

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from .models import Customer, Tender, Supplier

from pprint import pprint

chrome_driver = None
is_built = False


def chrome_driver_builder():
    global chrome_driver, is_built
    if is_built is False:
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_driver = webdriver.Chrome('/usr/local/bin/chromedriver', chrome_options=chrome_options)
        is_built = True


def parse_tender_by_url(url: str):
    chrome_driver_builder()
    chrome_driver.get(url)
    time.sleep(2)
    page = chrome_driver.page_source
    soup = BeautifulSoup(page)
    try:
        tender: Tender = _parse_tender(soup)
        tender.link = url
        tender.cast_tender_price_to_int()
    except:
        pass
    customer: Customer = _parse_customer(soup)
    suppliers = _parse_suppliers_by_offer(tender)
    return {
        "tender": tender,
        "customer": customer,
        "suppliers": suppliers
    }

def _parse_tender(soup: BeautifulSoup) -> Tender:
    try:
        tender: Tender = Tender()
        number_tag = soup.find(attrs={"class": "col ng-binding"})
        tender.number = number_tag.text
        name_tag = soup.find("span", attrs={"class": "text-m text-25 text-black ng-binding"})
        tender.name = name_tag.text
        price_tag = soup.find_all("td", attrs={"class": "ng-binding"})
        tender.price = price_tag[3].text
        return tender
    except Exception as e:
        print(f"Error while parse tender (parser.py, parser_tender(soup) - {e}")


def _parse_customer(soup: BeautifulSoup):
    try:
        customer = Customer()
        name_tag = soup.find(
            attrs={"ui-sref": "common.customers.emptyfilter.detail.view({id:cus.customer.id})", "class": "ng-binding"})
        customer.name = name_tag.text
        customer.link = 'https://old.zakupki.mos.ru/' + name_tag.get("href")
        chrome_driver.get(customer.link)
        time.sleep(2)
        page = chrome_driver.page_source
        soup = BeautifulSoup(page)
        # 5, 6, 7, 8, 9, 10
        try:
            info_tags = soup.find_all(attrs={"class": "ng-binding"})
            customer.inn = info_tags[5].text
            customer.kpp = info_tags[6].text
            customer.orgn = info_tags[7].text
            customer.phone_number = info_tags[8].text
            customer.mail = info_tags[9].text
            customer.site = info_tags[10].text
        except:
            pass
        return customer
    except Exception as e:
        print(f"Error while parse customer profile (parser.py, parser_customer(soup) - {e}")


def _parse_suppliers_by_offer(tender: Tender):
    offers = 10
    begin_of_url = "https://old.zakupki.mos.ru/#/sku?s.keyword="
    middle_of_url = tender.name.replace(" ", "%20")
    end_of_url = f"&s.state=actual&s.entityStateIn.0=1&v.ps={offers}&v.s=relevance&v.sd"
    complete_url = begin_of_url + middle_of_url + end_of_url
    chrome_driver.get(complete_url)
    wait = WebDriverWait(chrome_driver, 15)
    wait.until(EC.visibility_of_element_located(
        (By.CSS_SELECTOR, "#listView > div:nth-child(6) > div > div")))
    page = chrome_driver.page_source
    soup = BeautifulSoup(page)
    return _parse_active_offers(soup)


def _parse_active_offers(soup: BeautifulSoup):
    offers = soup.find_all("div", attrs={"class": "panel panel-default offer mb-4 ng-scope"})
    active_offers: List[Supplier] = []
    for element in offers:
        active_offer_price = element.find("p", attrs={"class": "price text-17 text-black text-m ng-binding ng-scope"})
        active_offer_name = element.find("a", attrs={"class": "o_name text-17 mb-3 ng-binding ng-scope"})
        temp_active_offer_price = None
        temp_active_offer_name = None
        temp_active_offer_link = None
        try:
            temp_active_offer_price = active_offer_price.text.strip()
            temp_active_offer_name = active_offer_name.text.strip()
            temp_active_offer_link = active_offer_name.get('href').strip()
        except:
            pass
        if temp_active_offer_link is not None and temp_active_offer_price is not None and temp_active_offer_name is not None:
            # print(temp_active_offer_link, temp_active_offer_price, temp_active_offer_name)
            chrome_driver.get(temp_active_offer_link)
            wait = WebDriverWait(chrome_driver, 10)
            wait.until(EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "div.middle.aligned.row.SkuOffersStyles__OfferRow-sc-11rqp4x-5")))
            page = chrome_driver.page_source
            active_offers += _parse_active_offer(temp_active_offer_name)
            # print(active_offers)
    return active_offers


def _parse_active_offer(temp_active_offer_name):
    suppliers: List[Supplier] = []
    for card in chrome_driver.find_elements_by_css_selector(
            "div.middle.aligned.row.SkuOffersStyles__OfferRow-sc-11rqp4x-5"):
        suppliers.append(Supplier())
        cards_text = [x for x in card.text.split("\n")]
        suppliers[-1].name = cards_text[0]
        suppliers[-1].inn = cards_text[1]
        suppliers[-1].tax = cards_text[5]
        suppliers[-1].price = cards_text[6]
        suppliers[-1].link = card.find_element_by_css_selector(
            "a.SkuOffersStyles__OfferSupplierLink-sc-11rqp4x-6").get_attribute("href")
        suppliers[-1].offer_descr = temp_active_offer_name
        # print(supplier)
        # for card in cards:1
        #     name = card.find("a", attrs={"class": "SkuOffersStyles__OfferSupplierLink-sc-11rqp4x-6 dSfepw"})
        #     description_tag = card.find_all("a",
        #                                     attrs={"class": "SkuOffersStyles__OfferDescriptionLink-sc-11rqp4x-7 iwDJGp"})
        #     print(name)
        #     pprint(description_tag)
    return suppliers


parse_tender_by_url(
    "https://old.zakupki.mos.ru/#/tenders/30916593?s.whithNegativeRating=false&s.compProc=false&s.aucNonStatObj=false&s.isRegisteredOss=false&s.stateIdIn.0=5&s.stateIdIn.1=2&v.ps=10&v.s=OrderDate&v.sd")
