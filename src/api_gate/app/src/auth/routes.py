from flask import render_template, request, redirect, url_for, flash, abort
from flask_login import login_user, logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from src.auth import bp
from src.models.user import User


@bp.route("/login", methods=["GET"])
def login_get():
    return render_template("auth/login.html")


@bp.route("/login", methods=["POST"])
def login_post():
    try:
        email = request.form.get("email")
        password = request.form.get("password")
        remember = True if request.form.get("remember") else False

        user = User.query.filter_by(email=email).first()

        if not user or not check_password_hash(user.password_hash, password):
            flash("Incorrect password or user does not exist")
            return redirect(url_for("auth.login_get"))

        login_user(user, remember=remember)
        return redirect(url_for("main.profile_get"))
    except Exception as e:
        abort(500)


@bp.route("/register", methods=["GET"])
def register_get():
    return render_template("auth/register.html")


@bp.route("/register", methods=["POST"])
def register_post():
    try:
        # get data from request
        email = request.form.get("email")
        username = request.form.get("username")
        password = request.form.get("password")

        # check is user exist in db

        user_from_db = User.query.filter_by(email=email).first()

        if user_from_db:
            flash("Email address already exists")
            return redirect("register")

        user_for_db = User(email=email,
                           username=username,
                           password_hash=generate_password_hash(password))
        if not user_for_db.save():
            raise Exception("Not successful user transaction")

        return redirect("login")
    except Exception as e:
        abort(500)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.index'))
