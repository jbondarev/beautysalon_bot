from flask import render_template, request, redirect, url_for
from flask_login import login_required, current_user
from src.main import bp


@bp.route('/')
def index():
    return render_template('main/index.html')


@bp.route('/profile_get', methods=["GET"])
@login_required
def profile_get():
    context: dict = {
        'name': current_user.username,
    }
    return render_template('main/profile.html', context=context)


@bp.route('/_calculate_suppliers', methods=['POST'])
@login_required
def _calculate_suppliers():
    url = request.form.get('url')
    from parselib import parser
    context = parser.parse_tender_by_url(url)
    return render_template('main/result.html', context=context)


@bp.route('/calculate_result', methods=['GET'])
@login_required
def calculate_result():
    return request.args['context']
