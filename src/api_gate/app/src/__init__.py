# flask imports
from flask_login import login_manager, LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import Flask
# local imports
from src.utils.os_utils import get_env_variable

# init sqlalchemy stuff

db: SQLAlchemy = SQLAlchemy()
migrate: Migrate = Migrate()


def create_app() -> Flask:
    """
    Flask factory
    :return:
    """

    app: Flask = Flask(__name__)
    register_blueprints(app)
    # get postgres variables
    POSTGRES_URL: str = get_env_variable("POSTGRES_URL")
    POSTGRES_USER: str = get_env_variable("POSTGRES_USER")
    POSTGRES_DB: str = get_env_variable("POSTGRES_DB")

    DB_URL: str = f"postgresql+psycopg2://{POSTGRES_USER}@{POSTGRES_URL}/{POSTGRES_DB}"
    # create sqlalch instance

    app.config['SECRET_KEY'] = '9OLWxND4o83j4K4iuopO'
    app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db.init_app(app)
    migrate.init_app(app, db)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login_get'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        from src.models.user import User
        return User.query.get(int(user_id))

    return app


def register_blueprints(app: Flask) -> None:
    from src.main import bp as MainBluePrint
    from src.auth import bp as AuthBluePrint
    app.register_blueprint(MainBluePrint)
    app.register_blueprint(AuthBluePrint)
